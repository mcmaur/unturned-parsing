// unturned.go

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"strconv"

	"github.com/BurntSushi/toml"
)

type Weapon struct {
	Id          string
	Name        string
	Category    string
	Damage      string
	Speed       string
	Slots       string
	Ammo        string
	Capacity    string
	Attachments string
	Firemodes   string
	Range       string
	Firerate    string
	Reloading   string
	Durability  string
	Equippable  string
	Stackable   string
	Weight      string
	Description string
}

func main() {
	dirpath := "./weapons/"
	fmt.Println("-------------- Working on " + dirpath + "--------------")
	files, _ := ioutil.ReadDir(dirpath)
	fmt.Println("There is " + strconv.Itoa(len(files)) + " files in the folder")

	var weapons = make([]Weapon, len(files))

	for i, _ := range files {
		fmt.Println("-x- " + files[i].Name())

		filepath := dirpath + files[i].Name()
		mdata, err := toml.DecodeFile(filepath, &weapons[i])
		if err != nil {
			fmt.Println("Error load toml " + files[i].Name() + ": " + err.Error())
			return
		}

		if len(mdata.Keys()) != lineCount(filepath) {
			fmt.Println("The number of key inside toml is " + strconv.Itoa(len(mdata.Keys())) + " instead the line inside the file is " + strconv.Itoa(lineCount(filepath)))
			return
		}
		if countValidField(weapons[i]) != lineCount(filepath) {
			fmt.Println("The number of valid field of the object is " + strconv.Itoa(countValidField(weapons[i])) + " instead the line inside the file is " + strconv.Itoa(lineCount(filepath)))
			fmt.Printf("%+v\n", weapons[i])
			return
		}
	}

	json, err := jsonize(weapons)
	if err != nil {
		fmt.Println("Error jsoning: ", err)
		return
	}
	fmt.Println("Final json is ", json)

	// csvfile, err := os.Create("weapons.json")
	f, err := os.Create("results/weapons.json")
	if err != nil {
		fmt.Println("Error opening file: ", err)
		return
	}
	defer f.Close()

	n3, err := f.WriteString(json)
	fmt.Printf("Wrote on file %d bytes\n", n3)

	f.Sync()

	fmt.Println("-- End " + dirpath + " --")
}

func countValidField(value interface{}) int {
	v := reflect.ValueOf(value)
	var validField int
	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).String() != "" {
			validField++
		}
	}
	return validField
}

func jsonize(things interface{}) (string, error) {
	jsoned, err := json.Marshal(things)
	if err != nil {
		fmt.Println("Errore: ", err)
		return "", err
	}
	return string(jsoned), nil
}

func lineCount(path string) int {
	file, _ := os.Open(path)
	fileScanner := bufio.NewScanner(file)
	lineCount := 0
	for fileScanner.Scan() {
		lineCount++
	}
	return lineCount
}
